class News < ActiveRecord::Base
  validates :title, :announce, presence: true
  scope :active, -> { where is_active: true }

end
