class Job < ActiveRecord::Base
  validates :title, :text, presence: true
  scope :active, -> { where is_active: true }
end
