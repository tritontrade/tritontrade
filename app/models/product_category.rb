class ProductCategory < ActiveRecord::Base
  validates :name, presence: true
  scope :active, -> { where is_active: true }  
  has_many :products, dependent: :destroy
end
