class Product < ActiveRecord::Base
  belongs_to :product_category
  has_many :product_relations
  has_many :factory_oils, :through => :product_relations
  has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" }
  
  def before_import_save(record)
    p record
    p "------" 
    
    if !ProductCategory.exists?(name: record[:product_category])
      ProductCategory.create name: record[:product_category], is_active: true
    end
  end
end
