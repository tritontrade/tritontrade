module ApplicationHelper
  def get_main_menu
    menu = Page.find_by_uri(:main_menu)
    menu.text.html_safe if menu
  end
  def get_footer_menu
    footer = Page.find_by_uri(:footer_menu)
    footer.text.html_safe if footer
  end
  def setting
    Setting.first! 
  end
end
