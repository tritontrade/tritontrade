class DeliveriesController < ApplicationController
  def get_price
    order_digit = Delivery.where("`to` = ? AND `from`= ? AND `truck` = ? ",
    params[:delivery][:to], params[:delivery][:from], params[:delivery][:truck]).first

    @delivery_summ = order_digit ? (order_digit.price_ton.to_i * order_digit.truck.to_i) : 0

  end
end
