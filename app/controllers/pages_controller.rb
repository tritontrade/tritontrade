class PagesController < ApplicationController
  def show
    @page = Page.find_by_uri!(params[:id])
    add_breadcrumb @page.name if @page
  end

  def contacts
    add_breadcrumb "Контакты"
  end
  
  def jobs
    add_breadcrumb "Карьера"
    @jobs = Job.all.active
  end
  
  def delivery
    add_breadcrumb "Доставка"
  end
end
