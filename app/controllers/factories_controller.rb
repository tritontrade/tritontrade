class FactoriesController < ApplicationController
  
  
  add_breadcrumb 'Заводы', :factories_path
  def index
    @factories = FactoryOil.all.active
  end
  def show
    @factory = FactoryOil.find_by_id!(params[:id])
    add_breadcrumb @factory.name if @factory
  end
end
