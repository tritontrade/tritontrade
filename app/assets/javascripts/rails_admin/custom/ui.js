//= require rich/base
//= require ckeditor/init
//= require ckeditor/config

CKEDITOR.editorConfig = function( config )
{
    config.toolbar_img = [
        { name: 'document', items: [ 'Source', '-', 'NewPage', 'Preview', '-', 'Templates' ] },
    ]
}
