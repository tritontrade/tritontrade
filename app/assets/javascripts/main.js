
        $(function () {
            var jcarousel = $('.jcarousel');

            jcarousel
                    .on('jcarousel:reload jcarousel:create', function () {
                        jcarousel.jcarousel('items').width(jcarousel.innerWidth());
                    })
                    .jcarousel({
                        wrap: 'circular',
                        transitions: Modernizr.csstransitions ? {
                            transforms:   Modernizr.csstransforms,
                            transforms3d: Modernizr.csstransforms3d,
                            easing:       'ease'
                        } : false
                    }).jcarouselAutoscroll({
                        interval: 4e3
                    })

            $('.jcarousel-control-prev')
                    .on('jcarouselcontrol:active', function() {
                        $(this).removeClass('inactive');
                    })
                    .on('jcarouselcontrol:inactive', function() {
                        $(this).addClass('inactive');
                    })
                    .jcarouselControl({
                        target: '-=1'
                    });

            $('.jcarousel-control-next')
                    .on('jcarouselcontrol:active', function() {
                        $(this).removeClass('inactive');
                    })
                    .on('jcarouselcontrol:inactive', function() {
                        $(this).addClass('inactive');
                    })
                    .on('click', function(e) {
                        e.preventDefault();
                    })
                    .jcarouselControl({
                        target: '+=1'
                    });

            $('.jcarousel-pagination')
                    .on('jcarouselpagination:active', 'a', function() {
                        $(this).addClass('active');
                    })
                    .on('jcarouselpagination:inactive', 'a', function() {
                        $(this).removeClass('active');
                    })
                    .on('click', function(e) {
                        e.preventDefault();
                    })
                    .jcarouselPagination({
                        item: function(page) {
                            return '<a href="#' + page + '">' + page + '</a>';
                        }
                    });
        });

        $(document).on('input', 'input.data_field', function(){
            var io = $(this).val().length ? 1 : 0 ;
            $(this).next('.icon_clear').stop().fadeTo(300,io);
        }).on('click', '.icon_clear', function() {
            $(this).delay(300).fadeTo(300,0).prev('input').val('');

        });

        $(document).on('input', 'input.data_field', function(){
            var io = $(this).val().length ? 1 : 0 ;
            $(this).next('.icon_clear').stop().fadeTo(300,io);
        }).on('click', '.icon_clear', function() {
            $(this).delay(300).fadeTo(300,0).prev('input').val('');

        });

        $(document).ready(function(){

            $('header .feedback-button').click(function(){
                $('header .popup-form.popup-callback').fadeIn();
                $('.shadow-fixed').fadeIn();
                return false;
            })
            $('footer .phone .btn-2').click(function(){
                $('footer .popup-form.popup-callback').fadeIn();
                $('.shadow-fixed').fadeIn();
                return false;
            })
            $(document).on("click", '.popup-form .close', function(){
                $(this).parent().fadeOut();
                $('.shadow-fixed').fadeOut();
                return false;
            })

            $('.shadow-fixed').click(function(){
                $('.popup-form').fadeOut();
                $(this).fadeOut();
            })
        })

