class CreateDeliveries < ActiveRecord::Migration
  def change
    create_table :deliveries do |t|
      t.string :article
      t.string :from
      t.string :to
      t.string :truck
      t.float :price_liter
      t.float :price_ton
      t.timestamps null: false
    end
  end
end
