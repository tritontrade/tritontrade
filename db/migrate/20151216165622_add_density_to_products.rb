class AddDensityToProducts < ActiveRecord::Migration
  def change
      add_column :products, :density, :string, default: 1
  end
end
