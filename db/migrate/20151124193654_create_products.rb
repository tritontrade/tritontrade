class CreateProducts < ActiveRecord::Migration
  def self.up
    create_table :products do |t|
      t.string :name
      t.string :article
      t.decimal :price_liter
      t.decimal :price_ton
      t.text :text
      t.timestamps null: false
      t.belongs_to :product_category, index: true
    end

    add_attachment :products, :image
  end


  def self.down
    drop_table :products
    remove_attachment :products, :image
  end
end
