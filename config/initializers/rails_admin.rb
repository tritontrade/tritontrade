
RailsAdmin.config do |config|

  ### Popular gems integration

  ## == Devise ==
  # config.authenticate_with do
  #   warden.authenticate! scope: :user
  # end
  # config.current_user_method(&:current_user)

  ## == Cancan ==
  config.authorize_with :cancan

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration
  #
  config.main_app_name = ['TritonTrade', 'Admin']
  config.included_models = ['Job','Setting','Delivery', 'News', 'Page', 'FactoryOil', 'Feedback', 'Product', 'ProductCategory']
  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    import do
      only ['Delivery', 'Product']
    end
    export
    bulk_delete
    show
    edit
    delete
    show_in_app
    ## With an audit adapter, you can add:
     history_index
     history_show
  end
  config.model Job do
    navigation_label 'Прочее'
      field :title
      field :text, :ck_editor
      field :is_active  
  end
  
  config.model Setting do
    navigation_label 'Прочее'
      field :site_name
      field :full_name
      field :address, :ck_editor
      field :full_address, :ck_editor
      field :work_time
      field :header_phone
      field :footer_phone
      field :order_phone
      field :manager_phones  , :ck_editor    
      field :keywords
      field :description      
  end
  
  config.model Feedback do
    navigation_label 'Прочее'
    field :title
    field :from
    field :to
    field :phone
    field :email
    field :text
  end
  config.model Page do
    navigation_label 'Информационные страницы'
    field :name
    field :uri
    field :text, :ck_editor
    field :is_active
    field :created_at
  end
  config.model News do
    navigation_label 'Информационные страницы'
    field :title
    field :announce, :ck_editor
    field :text, :ck_editor
    field :is_active
    field :created_at  
    configure :created_at do
      strftime_format '%Y-%m-%d'
    end 
  end
  config.model FactoryOil do
    navigation_label 'Информационные страницы'
    field :name
    field :fullname
    field :address
    field :phone
    field :about, :ck_editor
    field :image
    field :is_active
    field :is_main
  end
  
  config.model ProductCategory do
    navigation_label 'Продукты'
    field :name
    field :is_active
  end
  
  config.model Delivery do
    navigation_label 'Продукты'
    import do
      field :article
      field :from
      field :to
      field :truck
      field :price_liter
      field :price_ton
    end
  end
  
  config.model Product do
    navigation_label 'Продукты'
    import do
      field :product_category
      field :article
      field :name      
      field :price_liter
      field :price_ton
      field :density
      field :factory_oils do
        orderable true
      end
    end    
  end
end
