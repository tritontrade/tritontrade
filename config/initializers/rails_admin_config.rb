module RailsAdmin
  module Config
    module Actions
      class S3Link < Base
        RailsAdmin::Config::Actions.register(self)

        register_instance_option :member do
          true
        end

        register_instance_option :controller do
          Proc.new do
            redirect_to @object.s3_href
          end
        end
      end
    end
  end
end